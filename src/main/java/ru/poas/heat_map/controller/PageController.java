package ru.poas.heat_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.poas.heat_map.dto.PageDto;
import ru.poas.heat_map.dto.UrlDto;
import ru.poas.heat_map.exception.PageNotFoundException;
import ru.poas.heat_map.service.PageService;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/page")
public class PageController {

    private PageService pageService;

    @Autowired
    public PageController(PageService pageService) {
        this.pageService = pageService;
    }

    @GetMapping("/get/{id}")
    public ResponseEntity getPage(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(pageService.getPage(id), HttpStatus.OK);
        }
        catch (PageNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/getAllOnSite//{siteId}")
    public ResponseEntity getAllOnSite(@PathVariable(value = "siteId") Long id) {
        try {
            return new ResponseEntity<>(pageService.getAllOnSite(id), HttpStatus.OK);
        }
        catch (PageNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/getPageId")
    public ResponseEntity getPageId(@RequestBody UrlDto urlDto) {
        try {
            return new ResponseEntity<>(pageService.getPageBySiteIDAndUrl(urlDto.getSiteId(), urlDto.getUrl()), HttpStatus.OK);
        }
        catch (PageNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }


    @PostMapping("/add")
    public ResponseEntity create(@RequestBody PageDto pageDto) {
        try {
            return new ResponseEntity<>(pageService.savePage(pageDto), HttpStatus.CREATED);
        }
        catch (DataIntegrityViolationException e) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
    }
    @PutMapping("/update")
    public ResponseEntity<PageDto> update(@Valid @RequestBody PageDto pageDto) {
        return new ResponseEntity<>(pageService.update(pageDto), HttpStatus.CREATED);
    }


    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try {
            pageService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (org.springframework.dao.EmptyResultDataAccessException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }
}
