package ru.poas.heat_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.poas.heat_map.exception.SessionFrameNotFoundException;
import ru.poas.heat_map.service.SessionFrameService;

@RestController
@RequestMapping(value = "/sessionFrame")
public class SessionFrameController {

    private SessionFrameService sessionFrameService;

    @Autowired
    public SessionFrameController(SessionFrameService sessionFrameService) {
        this.sessionFrameService = sessionFrameService;
    }

    @GetMapping(value = "/get/{sessionId}")
    public ResponseEntity getSessionFrames(@PathVariable(value = "sessionId") Long sessionId) {
        try {
            return new ResponseEntity<>(sessionFrameService.getSessionFrames(sessionId), HttpStatus.OK);
        }
        catch (SessionFrameNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/getByPage/{pageId}")
    public ResponseEntity getSessionFramesByPageId(@PathVariable(value = "pageId") Long pageId) {
        try {
            return new ResponseEntity<>(sessionFrameService.getSessionFramesByPage(pageId), HttpStatus.OK);
        }
        catch (SessionFrameNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }
    @GetMapping(value = "/getAll")
    public ResponseEntity getAllSessionFrames() {
        try {
            return new ResponseEntity<>(sessionFrameService.getAllSessionFrames(), HttpStatus.OK);
        }
        catch (SessionFrameNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping(value = "/getAllBySessionIdAndPageId/{sessionId}/{pageId}")
    public ResponseEntity getAllSessionFramesBySessionIdAndPageId(@PathVariable(value = "sessionId") Long sessionId, @PathVariable(value = "pageId") Long pageId) {
        try {
            return new ResponseEntity<>(sessionFrameService.getAllSessionFramesBySessionIdAndPageId(sessionId, pageId), HttpStatus.OK);
        }
        catch (SessionFrameNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try {
            sessionFrameService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (org.springframework.dao.EmptyResultDataAccessException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }
}
