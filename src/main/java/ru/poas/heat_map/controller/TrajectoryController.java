package ru.poas.heat_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.poas.heat_map.dto.session.SessionFrameTrajectoryDto;
import ru.poas.heat_map.exception.TrajectoryNotFoundException;
import ru.poas.heat_map.service.TrajectoryService;

import java.util.List;

@RestController
@RequestMapping(value = "/trajectory")
public class TrajectoryController {

    private TrajectoryService trajectoryService;

    @Autowired
    public TrajectoryController(TrajectoryService trajectoryService) {
        this.trajectoryService = trajectoryService;
    }

    @PostMapping("/save")
    public ResponseEntity saveTrajectory(@RequestBody List<SessionFrameTrajectoryDto> sessionFrameTrajectoryDto) {
        trajectoryService.saveTrajectory(sessionFrameTrajectoryDto);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @GetMapping("/get/{sessionFrameId}")
    public ResponseEntity getTrajectory(@PathVariable(value = "sessionFrameId") Long sessionFrameId) {
        try {
            return new ResponseEntity<>(trajectoryService.getTrajectory(sessionFrameId), HttpStatus.OK);
        }
        catch (TrajectoryNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }
}