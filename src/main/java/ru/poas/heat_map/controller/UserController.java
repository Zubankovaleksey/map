package ru.poas.heat_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.poas.heat_map.dto.UserDto;
import ru.poas.heat_map.exception.UserNotFoundException;
import ru.poas.heat_map.service.UserService;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/get/all")
    public ResponseEntity getAllUsers() {
        try {
            return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
        }
        catch (UserNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/get/{id}")
    public ResponseEntity getUser(@PathVariable(value = "id") Long id) {
        try {
            return new ResponseEntity<>(userService.getUser(id), HttpStatus.OK);
        }
        catch (UserNotFoundException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/add")
    public ResponseEntity addUser(@RequestBody UserDto userDto) {
        try {
            return new ResponseEntity<>(userService.addUser(userDto), HttpStatus.CREATED);
        }
        catch (DataIntegrityViolationException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable(value = "id") Long id) {
        try {
            userService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (org.springframework.dao.EmptyResultDataAccessException e) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

}
