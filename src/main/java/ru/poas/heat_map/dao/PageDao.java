package ru.poas.heat_map.dao;

import org.springframework.data.repository.CrudRepository;
import ru.poas.heat_map.model.Page;

import java.util.List;

public interface PageDao extends CrudRepository<Page, Long> {

    Page findBySiteIDAndUrl(Long siteId, String url);

    List<Page> findAllBySiteID(Long siteId);

}
