package ru.poas.heat_map.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.poas.heat_map.model.User;

@Repository
public interface UserDao extends CrudRepository<User, Long> {

}
