package ru.poas.heat_map.dao.session;

import org.springframework.data.repository.CrudRepository;
import ru.poas.heat_map.model.session.Session;

import java.util.List;

public interface SessionDao extends CrudRepository<Session, Long> {

    List<Session> findAllBySiteId(Long siteId);
}
