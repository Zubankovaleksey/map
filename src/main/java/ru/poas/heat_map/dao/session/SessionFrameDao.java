package ru.poas.heat_map.dao.session;

import org.springframework.data.repository.CrudRepository;
import ru.poas.heat_map.model.session.SessionFrame;

import java.util.List;
import java.util.Optional;

public interface SessionFrameDao extends CrudRepository<SessionFrame, Long> {

    Optional<List<SessionFrame>> findBySessionId(Long sessionId);
    Optional<List<SessionFrame>> findByPageId(Long pageId);

    List<SessionFrame> findAllBySessionIdAndPageId(Long sessionId, Long pageId);
}
