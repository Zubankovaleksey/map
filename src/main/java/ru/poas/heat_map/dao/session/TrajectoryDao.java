package ru.poas.heat_map.dao.session;

import org.springframework.data.repository.CrudRepository;
import ru.poas.heat_map.model.session.Trajectory;

import java.util.List;
import java.util.Optional;

public interface TrajectoryDao extends CrudRepository<Trajectory, Long> {

    List<Trajectory> getBySessionFrameId(Long sessionFrameId);
}
