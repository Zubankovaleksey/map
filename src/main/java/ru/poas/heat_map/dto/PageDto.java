package ru.poas.heat_map.dto;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import net.minidev.json.JSONArray;
import org.hibernate.annotations.TypeDef;
import ru.poas.heat_map.model.Page;

@NoArgsConstructor
@ToString
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class PageDto {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String url;

    @Getter
    @Setter
    private  Long siteId;

    @Getter
    @Setter
    private JSONArray priority;

    @Getter
    @Setter
    private Boolean isStartPage;

    public PageDto(Page page)
    {
        this.id = page.getId();
        this.name = page.getName();
        this.url = page.getUrl();
        this.siteId = page.getSiteID();
        this.isStartPage = page.getIsStartPage();
        this.priority = page.getPriority();
    }

}
