package ru.poas.heat_map.dto;

import lombok.Getter;
import lombok.Setter;

public class UrlDto {

    @Getter
    @Setter
    private String url;

    @Getter
    @Setter
    private  Long siteId;

    public UrlDto(String url, Long siteId) {
        this.url = url;
        this.siteId = siteId;
    }
}
