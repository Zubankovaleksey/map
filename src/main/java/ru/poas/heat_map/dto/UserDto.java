package ru.poas.heat_map.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.poas.heat_map.model.User;


import java.time.LocalDate;

@NoArgsConstructor
@ToString
public class UserDto {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String surname;

    @Getter
    @Setter
    private String patronymic;

    @Getter
    @Setter
    private LocalDate dateOfBirth;

    @Getter
    @Setter
    private Boolean gender;

    public UserDto(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.patronymic = user.getPatronymic();
        this.dateOfBirth = user.getDateOfBirth();
        this.gender = user.getGender();
    }
}
