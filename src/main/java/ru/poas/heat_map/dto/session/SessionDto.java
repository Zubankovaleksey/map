package ru.poas.heat_map.dto.session;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.poas.heat_map.model.session.Session;

@NoArgsConstructor
@ToString
public class SessionDto {

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private Long userId;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private int pageShowTime;

    @Getter
    @Setter
    private Long siteId;

    public SessionDto(Session session) {
        this.id = session.getId();
        this.userId = session.getUserId();
        this.name = session.getName();
        this.pageShowTime = session.getPageShowTime();
        this.siteId = session.getSiteId();
    }
}
