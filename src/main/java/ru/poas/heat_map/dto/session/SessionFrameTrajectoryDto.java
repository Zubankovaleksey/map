package ru.poas.heat_map.dto.session;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.poas.heat_map.model.session.Trajectory;

import java.util.List;

@NoArgsConstructor
@ToString
public class SessionFrameTrajectoryDto {

    @Getter
    @Setter
    private SessionFrameDto sessionFrame;

    @Getter
    @Setter
    private List<Trajectory> trajectoryList;
}
