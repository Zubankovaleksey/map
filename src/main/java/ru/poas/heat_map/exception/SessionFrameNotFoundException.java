package ru.poas.heat_map.exception;

public class SessionFrameNotFoundException extends RuntimeException {
    public SessionFrameNotFoundException(String message) {
        super(message);
    }
}
