package ru.poas.heat_map.exception;

public class TrajectoryNotFoundException extends RuntimeException {
    public TrajectoryNotFoundException(String message) {
        super(message);
    }
}
