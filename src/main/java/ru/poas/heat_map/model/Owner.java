package ru.poas.heat_map.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;


@NoArgsConstructor
@Entity
@Table(name = "owner", uniqueConstraints = @UniqueConstraint(columnNames = {"NICKNAME", "LOGIN"}))
public class Owner {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @Column(name="NICKNAME", nullable = false)
    private String nickname;

    @Getter
    @Setter
    @Column(name="LOGIN", nullable = false)
    private String login;

    @Getter
    @Setter
    @Column(name = "PASSWORD", nullable = false)
    private String password;

}
