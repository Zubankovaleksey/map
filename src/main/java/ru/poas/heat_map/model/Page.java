package ru.poas.heat_map.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.minidev.json.JSONArray;
import org.hibernate.annotations.Type;
import ru.poas.heat_map.dto.PageDto;

import javax.persistence.*;

@NoArgsConstructor
@Entity
@Table(name = "PAGE")
public class Page {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @Column(name="NAME", nullable = false)
    private String name;

    @Getter
    @Setter
    @Column(name="URL", nullable = false)
    private String url;

    @Getter
    @Setter
    @Column(name="SITE_ID", nullable = false)
    private Long siteID;

    @Getter
    @Setter
    @Column(name="IS_START_PAGE", nullable = false)
    private Boolean isStartPage;

    @Getter
    @Setter
    @Type( type = "json" )
    @Column(name = "PRIORITY", columnDefinition = "json" )
    private JSONArray priority;

    public Page (PageDto pageDto)
    {
        this.name = pageDto.getName();
        this.siteID = pageDto.getSiteId();
        this.url = pageDto.getUrl();
        this.isStartPage = pageDto.getIsStartPage();
        this.priority = pageDto.getPriority();
    }

    public PageDto toDto() {
        PageDto pageDto = new PageDto();
        pageDto.setId(this.id);
        pageDto.setName(this.name);
        pageDto.setUrl(this.url);
        pageDto.setSiteId(this.siteID);
        pageDto.setIsStartPage(this.isStartPage);
        pageDto.setPriority(this.priority);
        return pageDto;
    }

    public void update(PageDto pageDto) {
        this.setId(pageDto.getId());
        if (pageDto.getName() != null) {this.setName(pageDto.getName());}
        if (pageDto.getSiteId() != null) {this.setSiteID(pageDto.getSiteId());}
        if (pageDto.getUrl() != null) {this.setUrl(pageDto.getUrl());}
        if (pageDto.getIsStartPage() != null) {this.setIsStartPage(pageDto.getIsStartPage());}
        if (pageDto.getPriority() != null) {this.setPriority(pageDto.getPriority());}
    }
}
