package ru.poas.heat_map.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.poas.heat_map.dto.UserDto;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
public class User {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    @Setter
    @Column(name="NAME", nullable = false)
    private String name;

    @Getter
    @Setter
    @Column(name="SURNAME", nullable = false)
    private String surname;

    @Getter
    @Setter
    @Column(name="PATRONYMIC", nullable = false)
    private String patronymic;

    @Getter
    @Setter
    @Column(name = "DATE_OF_BIRTH", nullable = false)
    private LocalDate dateOfBirth;

    @Getter
    @Setter
    @Column(name = "GENDER", nullable = false)
    private Boolean gender;

    public User(UserDto userDto) {
        this.name = userDto.getName();
        this.surname = userDto.getSurname();
        this.patronymic = userDto.getPatronymic();
        this.dateOfBirth = userDto.getDateOfBirth();
        this.gender = userDto.getGender();
    }
}
