package ru.poas.heat_map.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.poas.heat_map.dao.PageDao;
import ru.poas.heat_map.dto.PageDto;
import ru.poas.heat_map.exception.PageNotFoundException;
import ru.poas.heat_map.model.Page;

import java.util.ArrayList;
import java.util.List;

@Service
public class PageService {

    private PageDao pageDao;

    @Autowired
    public PageService(PageDao pageDao) {
        this.pageDao = pageDao;
    }

    public Page getPage(Long id) {
        return pageDao.findById(id).orElseThrow(()-> new PageNotFoundException("Page with id " + id + " not found."));
    }

    public PageDto savePage(PageDto pageDto) {
        return new PageDto(pageDao.save(new Page(pageDto)));
    }

    public PageDto getPageBySiteIDAndUrl(Long siteId, String url) {
        return new PageDto(pageDao.findBySiteIDAndUrl(siteId, url));
    }

    public List<Page> getAllOnSite(Long siteId) {
        List<Page> pages = new ArrayList<>();
        pageDao.findAllBySiteID(siteId).forEach(pages::add);
        if (pages.isEmpty())
            throw new PageNotFoundException("Session list is empty.");
        return pages;
    }

    public void delete(Long id) {
        pageDao.deleteById(id);
    }


    public PageDto update(PageDto pageDto)
    {
        Page page = pageDao.findById(pageDto.getId()).orElseThrow(() -> new PageNotFoundException("Page with ID " + pageDto.getId() + " not found."));
        page.update(pageDto);
        return pageDao.save(page).toDto();
    }}
