package ru.poas.heat_map.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.poas.heat_map.dao.session.SessionFrameDao;
import ru.poas.heat_map.dto.session.SessionFrameDto;
import ru.poas.heat_map.exception.SessionFrameNotFoundException;

import java.util.ArrayList;
import java.util.List;

@Service
public class SessionFrameService {

    private SessionFrameDao sessionFrameDao;

    @Autowired
    public SessionFrameService(SessionFrameDao sessionFrameDao) {
        this.sessionFrameDao = sessionFrameDao;
    }

    public List<SessionFrameDto> getSessionFrames(Long sessionId) {
        List<SessionFrameDto> sessionFrameDtoList = new ArrayList<>();
        sessionFrameDao.findBySessionId(sessionId)
                .orElseThrow(()->new SessionFrameNotFoundException("Session with id " + sessionId + " has no frames."))
                .forEach(e-> sessionFrameDtoList.add(new SessionFrameDto(e)));
        return sessionFrameDtoList;
    }
    public List<SessionFrameDto> getSessionFramesByPage(Long pageId) {
        List<SessionFrameDto> sessionFrameDtoList = new ArrayList<>();
        sessionFrameDao.findByPageId(pageId)
                .orElseThrow(()->new SessionFrameNotFoundException("Session with id " + pageId + " has no frames."))
                .forEach(e-> sessionFrameDtoList.add(new SessionFrameDto(e)));
        return sessionFrameDtoList;
    }


    public void delete(Long id) {
        sessionFrameDao.deleteById(id);
    }

    public List<SessionFrameDto> getAllSessionFrames() {
        List<SessionFrameDto> sessionFrameDtoList = new ArrayList<>();
        sessionFrameDao.findAll()
                .forEach(e-> sessionFrameDtoList.add(new SessionFrameDto(e)));
        return sessionFrameDtoList;
    }

    public List<SessionFrameDto> getAllSessionFramesBySessionIdAndPageId(Long sessionId, Long pageId) {
        List<SessionFrameDto> sessionFrameDtoList = new ArrayList<>();
        sessionFrameDao.findAllBySessionIdAndPageId(sessionId, pageId)
                .forEach(e-> sessionFrameDtoList.add(new SessionFrameDto(e)));
        return sessionFrameDtoList;
    }
}
