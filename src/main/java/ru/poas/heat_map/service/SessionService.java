package ru.poas.heat_map.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.poas.heat_map.dao.session.SessionDao;

import ru.poas.heat_map.dto.session.SessionDto;
import ru.poas.heat_map.exception.SessionNotFoundException;
import ru.poas.heat_map.model.session.Session;

import java.util.ArrayList;
import java.util.List;

@Service
public class SessionService {

    private SessionDao sessionDao;

    @Autowired
    public SessionService(SessionDao sessionDao) {
        this.sessionDao = sessionDao;
    }

    public Session getSession(Long id) {
        return sessionDao.findById(id)
                .orElseThrow(()-> new SessionNotFoundException("Session with id " + id + " not found."));
    }

    public List<Session> getAllSessions() {
        List<Session> sessions = new ArrayList<>();
        sessionDao.findAll().forEach(sessions::add);
        if (sessions.isEmpty())
            throw new SessionNotFoundException("Session list is empty.");
        return sessions;
    }

    public SessionDto createSession(SessionDto sessionDto) {
        return new SessionDto(sessionDao.save(new Session(sessionDto)));
    }


    public void delete(Long id) {
        sessionDao.deleteById(id);
    }

    public List<Session> getSessionsBySiteId(Long siteId) {
        List<Session> sessions = new ArrayList<>();
        sessionDao.findAllBySiteId(siteId).forEach(sessions::add);
        if (sessions.isEmpty())
            throw new SessionNotFoundException("Session list is empty.");
        return sessions;
    }
}
