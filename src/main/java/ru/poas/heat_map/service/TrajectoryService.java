package ru.poas.heat_map.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.poas.heat_map.dao.session.SessionFrameDao;
import ru.poas.heat_map.dao.session.TrajectoryDao;
import ru.poas.heat_map.dto.session.SessionFrameDto;
import ru.poas.heat_map.dto.session.SessionFrameTrajectoryDto;
import ru.poas.heat_map.exception.SessionNotFoundException;
import ru.poas.heat_map.exception.TrajectoryNotFoundException;
import ru.poas.heat_map.model.session.Session;
import ru.poas.heat_map.model.session.SessionFrame;
import ru.poas.heat_map.model.session.Trajectory;

import java.util.ArrayList;
import java.util.List;

@Service
public class TrajectoryService {

    private SessionFrameDao sessionFrameDao;
    private TrajectoryDao trajectoryDao;

    @Autowired
    public TrajectoryService(SessionFrameDao sessionFrameDao, TrajectoryDao trajectoryDao) {
        this.sessionFrameDao = sessionFrameDao;
        this.trajectoryDao = trajectoryDao;
    }

    private SessionFrameDto saveSessionFrame(SessionFrameDto sessionFrame) {
        return new SessionFrameDto(sessionFrameDao.save(new SessionFrame(sessionFrame)));
    }

    public void saveTrajectory(List<SessionFrameTrajectoryDto> sessionFrameTrajectoryDtoList) {

        for (SessionFrameTrajectoryDto item: sessionFrameTrajectoryDtoList) {
            SessionFrameDto sessionFrameDto = saveSessionFrame(item.getSessionFrame());
            item.getTrajectoryList().forEach(e-> e.setSessionFrameId(sessionFrameDto.getId()));
            trajectoryDao.saveAll(item.getTrajectoryList());
        }
    }

    public List<Trajectory> getTrajectory(Long sessionFrameId) {
        List<Trajectory> traectories = new ArrayList<>();
        trajectoryDao.getBySessionFrameId(sessionFrameId).forEach(traectories::add);
        if (traectories.isEmpty())
            throw new SessionNotFoundException("Session list is empty.");
        return traectories;
    }
}
