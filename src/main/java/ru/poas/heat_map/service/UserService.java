package ru.poas.heat_map.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.poas.heat_map.dao.UserDao;
import ru.poas.heat_map.dto.UserDto;
import ru.poas.heat_map.exception.UserNotFoundException;
import ru.poas.heat_map.model.User;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private UserDao userDao;

    @Autowired
    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public List<UserDto> getAllUsers() {
        List<UserDto> users = new ArrayList<>();
        userDao.findAll().forEach(e-> users.add(new UserDto(e)));
        if (users.isEmpty())
            throw new UserNotFoundException("User list is empty.");
        return users;
    }
    public void delete(Long id) {
        userDao.deleteById(id);
    }

    public UserDto getUser(Long id) {
        return new UserDto(userDao.findById(id)
                .orElseThrow(()-> new UserNotFoundException("User with id " + id + " not found.")));
    }

    public UserDto addUser(UserDto userDto) {
        return new UserDto(userDao.save(new User(userDto)));
    }
}
